#conta quantas cebolas tem
cebolas = 450
#conta quantas cebolas tem nas caixas
cebolas_na_caixa = 180
#conta quantas cebolas podem em uma caixa
espaco_caixa = 5
#conta quantas caixas tem
caixas = 30
#conta quantas cebolas tem fora das caixas
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#conta quantas caixas estão vazias
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#conta quantas caixas são necessarias para completar as cebolas
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
#printa quantas cebolas estão encaixadas
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
#printa quantas cebolas não estão encaixadas
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
#printa quantas cebolas podem ir em cada caixa
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
#printa quantas caixa ainda tem
print ("Ainda temos,", caixas_vazias, "caixas vazias")
#printa quantas caixa faltam
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")

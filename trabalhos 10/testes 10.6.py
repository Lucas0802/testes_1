from itertools import takewhile, count

cubes1to100 = list(takewhile(lambda x: x <= 100, map(lambda x: x**3, count())))
print (cubes1to100)
